/*
 * Copyright 2012 Jonathan McDowell <noodles@earth.li>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <string.h>
#include <libgen.h>
#include <libmtp.h>
#include <tag_c.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>

#include "files.h"
#include "mtp.h"

static LIBMTP_mtpdevice_t *devicelist = NULL;
static LIBMTP_mtpdevice_t *device = NULL;

static void mtp_marktrack(LIBMTP_track_t *track)
{
	struct track *file_track;

	file_track = files_findtrack(track->artist, track->album,
			track->title);

	if (file_track) {
		printf("\rMarking %s / %s as as seen.              ",
				track->artist, track->title);
		file_track->seen = 1;
	} else {
		/* If delete set, delete track from device */
	}
}

int mtp_add_track_to_album(LIBMTP_album_t *albuminfo,
		LIBMTP_track_t *trackmeta)
{
	LIBMTP_album_t *album;
	LIBMTP_album_t *found_album = NULL;
	int ret;

	/* Look for the album */
	album = LIBMTP_Get_Album_List(device);
	while (album != NULL) {
		if (album->name != NULL && album->artist != NULL &&
				!strcmp(album->name, albuminfo->name) &&
				!strcmp(album->artist, albuminfo->artist)) {
			/* Disconnect this album for later use */
			found_album = album;
			album = album->next;
			found_album->next = NULL;
		} else {
			LIBMTP_album_t *tmp;

			tmp = album;
			album = album->next;
			LIBMTP_destroy_album_t(tmp);
		}
  	}
  
	if (found_album != NULL) {
		uint32_t *tracks;

		tracks = (uint32_t *) malloc((found_album->no_tracks+1) *
				sizeof(uint32_t));
		printf("Album \"%s\" found: updating...\n", found_album->name);
		if (!tracks) {
			printf("failed malloc in add_track_to_album()\n");
			return 1;
		}
		found_album->no_tracks++;
		if (found_album->tracks != NULL) {
			memcpy(tracks, found_album->tracks,
				found_album->no_tracks * sizeof(uint32_t));
			free(found_album->tracks);
		}
		tracks[found_album->no_tracks-1] = trackmeta->item_id;
		found_album->tracks = tracks;
		ret = LIBMTP_Update_Album(device, found_album);
		LIBMTP_destroy_album_t(found_album);
	} else {
		uint32_t *trackid;

		trackid = (uint32_t *) malloc (sizeof(uint32_t));
		*trackid = trackmeta->item_id;
		albuminfo->tracks = trackid;
		albuminfo->no_tracks = 1;
		printf("Album doesn't exist: creating...\n");
		ret = LIBMTP_Create_New_Album(device, albuminfo);
		/* albuminfo will be destroyed later by caller */
	}
  
	if (ret != 0) {
		printf("Error creating or updating album.\n");
		LIBMTP_Dump_Errorstack(device);
		LIBMTP_Clear_Errorstack(device);
	} else {
		printf("success!\n");
	}

	return 0;
}

LIBMTP_filetype_t taglibtype_to_mtptype(TagLib_File_Type type)
{
	switch (type) {
	case TagLib_File_MPEG:
		return LIBMTP_FILETYPE_MP3;
	case TagLib_File_OggVorbis:
		return LIBMTP_FILETYPE_OGG;
	case TagLib_File_FLAC:
	case TagLib_File_OggFlac:
		return LIBMTP_FILETYPE_FLAC;
	default:
		printf("Unmappable taglib file type: %d\n", type);
		return LIBMTP_FILETYPE_UNKNOWN;
	}
}

int mtp_sendtrack(struct track *track)
{
	char *filename, *basefile;
	uint32_t parent_id = 0;
	LIBMTP_track_t *trackmeta;
	LIBMTP_album_t *albuminfo;
	int ret;

	printf("Sending %s to MTP device.\n", track->filename);
	filename = strdup(track->filename);
	basefile = basename(filename);

	trackmeta = LIBMTP_new_track_t();
	albuminfo = LIBMTP_new_album_t();

//	parent = dirname(to_path);
//	filename = basename(to_path);
//	parent_id = parse_path (parent,files,folders);
	parent_id = 0;
	if (parent_id == -1) {
		printf("Parent folder could not be found, skipping\n");
		return 1;
	}

	trackmeta->filetype = taglibtype_to_mtptype(TagLib_File_MPEG);
	if (trackmeta->filetype == LIBMTP_FILETYPE_UNKNOWN) {
		printf("Not a valid codec: \"%s\"\n",
		LIBMTP_Get_Filetype_Description(trackmeta->filetype));
		printf("Supported formats: MP3, WAV, OGG, MP4, AAC, "
				"M4A, FLAC, WMA\n");
		return 1;
	}

	printf("Sending track:\n");
	printf("Codec:     %s\n",
		LIBMTP_Get_Filetype_Description(trackmeta->filetype));
	if (track->title) {
		trackmeta->title = strdup(track->title);
	}
	if (track->album) {
		trackmeta->album = strdup(track->album);
		albuminfo->name = strdup(track->album);
	}
	if (track->artist) {
		trackmeta->artist = strdup(track->artist);
		albuminfo->artist = strdup(track->artist);
	}
	trackmeta->tracknumber = track->tracknum;
	/*
	 * TODO:
	 * year, length, genre
	 */
	if (filename != NULL) {
		trackmeta->filename = strdup(basefile);
	}
	trackmeta->filesize = track->size;
     
	printf("Sending track...\n");
	ret = LIBMTP_Send_Track_From_File(device, track->filename,
			trackmeta, NULL, NULL);
	printf("\n");
	if (ret != 0) {
		printf("Error sending track.\n");
		LIBMTP_Dump_Errorstack(device);
		LIBMTP_Clear_Errorstack(device);
	} else {
		printf("New track ID: %d\n", trackmeta->item_id);
	}

	LIBMTP_destroy_album_t(albuminfo);
	LIBMTP_destroy_track_t(trackmeta);

	return 0;
}

int mtp_buildtracklist(void)
{
	LIBMTP_track_t *tracks;

	/* Get track listing. */
	tracks = LIBMTP_Get_Tracklisting_With_Callback(device, NULL, NULL);
	if (tracks == NULL) {
		printf("No tracks.\n");
	} else {
		LIBMTP_track_t *track, *tmp;
		track = tracks;
		while (track != NULL) {
			/*
			 * If it's a music file, mark it.
			 */
			if (track->filetype == LIBMTP_FILETYPE_MP3 ||
				track->filetype == LIBMTP_FILETYPE_OGG ||
				track->filetype == LIBMTP_FILETYPE_WMA) {
				mtp_marktrack(track);
			}
			tmp = track;
			track = track->next;
			LIBMTP_destroy_track_t(tmp);
		}
		printf("\n");
	}

	return 0;
}

int mtp_init(char *devicename)
{
	char *friendlyname;

	LIBMTP_Init();
	printf("Attempting to connect MTP device.\n");

	switch(LIBMTP_Get_Connected_Devices(&devicelist)) {
	case LIBMTP_ERROR_NO_DEVICE_ATTACHED:
		printf("No MTP devices found.\n");
		return 1;
	case LIBMTP_ERROR_CONNECTING:
		printf("Error connecting to MTP device.\n");
		return 1;
	case LIBMTP_ERROR_MEMORY_ALLOCATION:
		printf("libmtp memory allocation error.\n");
		return 1;
 
	/* Unknown general errors - This should never execute */
	case LIBMTP_ERROR_GENERAL:
	default:
		printf("Unknown error, please report "
	                    "this to the libmtp developers\n");
		return 1;

	/* Successfully connected at least one device, so continue */
	case LIBMTP_ERROR_NONE:
		printf("Successfully connected to MTP device: ");
	}
  
	for (device = devicelist; device != NULL; device = device->next) {
		if (devicename == NULL || devicename[0] == '\0') {
			friendlyname = LIBMTP_Get_Friendlyname(device);
			printf("%s\n", friendlyname);
			free(friendlyname);
			break;
		}
	    	friendlyname = LIBMTP_Get_Friendlyname(device);
		if (strcmp(friendlyname, devicename) == 0) {
			printf("Found: %s\n", friendlyname ? friendlyname :
					"(NULL)");
			free(friendlyname);
			break;
		}
		free(friendlyname);
	}

	return (device != NULL) ? 0 : 1;
}

void mtp_cleanup(void)
{
	LIBMTP_Release_Device_List(devicelist);
}
