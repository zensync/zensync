/*
 * Copyright 2012 Jonathan McDowell <noodles@earth.li>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <dirent.h>
#include <tag_c.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "files.h"

static struct track *tracks = NULL;
static int trackcount = 0;
static int tracksize = 0;

static int trackcmp(const struct track *a, const struct track *b)
{
	int res;

	res = strcmp(a->artist, b->artist);
	if (res == 0) {
		res = strcmp(a->album, b->album);
	}
	if (res == 0) {
		res = strcmp(a->title, b->title);
	}

	return res;
}

static int files_inserttrack(struct track *track)
{
	int startpos, endpos, curpos;
	int res;

	if (trackcount == 0) {
		tracks[trackcount] = *track;
		trackcount++;
		return 0;
	}

	if (tracksize <= trackcount) {
		tracksize *= 2;
		tracks = realloc(tracks,
			sizeof(struct track) * tracksize);
		printf("Increasing tracks to %d\n", tracksize);
	}

	startpos = 0;
	endpos = trackcount - 1;
	curpos = (endpos - startpos) / 2;

	while (startpos <= endpos) {
		curpos = (startpos + endpos) / 2;
		res = trackcmp(track, &tracks[curpos]);

		if (res > 0) {
			startpos = curpos + 1;
		} else if (res < 0) {
			endpos = curpos - 1;
		} else {
			printf("Duplicate track!\n");
			return 1;
		}
	}

	if (res > 0) {
		curpos++;
	}

	memmove(&tracks[curpos + 1], &tracks[curpos],
			sizeof (struct track) * (trackcount - curpos));
	tracks[curpos] = *track;
	trackcount++;

	return 0;
}

static void files_addtrack(const char *file)
{
	struct stat statbuf;
	TagLib_File *tlfile;
	TagLib_Tag *tag;
	struct track newtrack;

	stat(file, &statbuf);

	tlfile = taglib_file_new(file);
	if (!tlfile || !taglib_file_is_valid(tlfile)) {
		printf("Error opening %s\n", file);
		return;
	}
	tag = taglib_file_tag(tlfile);

	if (strlen(taglib_tag_title(tag)) != 0) {
		newtrack.filename = strdup(file);
		newtrack.size = statbuf.st_size;
		newtrack.seen = 0;
		newtrack.title = strdup(taglib_tag_title(tag));
		newtrack.artist = strdup(taglib_tag_artist(tag));
		newtrack.album = strdup(taglib_tag_album(tag));
		newtrack.tracknum = taglib_tag_track(tag);

		if (!files_inserttrack(&newtrack)) {
			printf("Added track %d (%s)\n", trackcount, file);
		}
	}

	taglib_tag_free_strings();
	taglib_file_free(tlfile);

	return;
}

void files_gettracks(const char *dir)
{
	DIR *curdir;
	struct dirent *entry;
	struct stat statbuf;
	char fullpath[PATH_MAX];

	curdir = opendir(dir);
	if (curdir == NULL) {
		perror("Couldn't open file:");
		return;
	}
	while ((entry = readdir(curdir)) != NULL) {
		snprintf(fullpath, sizeof(fullpath), "%s/%s",
			dir,
			entry->d_name);
		if (!strcmp(entry->d_name, ".") ||
				!strcmp(entry->d_name, "..")) {
		} else if (entry->d_type == DT_DIR) {
			files_gettracks(fullpath);
		} else if (entry->d_type == DT_REG) {
			/* It's a file. Add it. */
			files_addtrack(fullpath);
		} else if (entry->d_type == DT_UNKNOWN) {
			/* Filesystem doesn't tell us with readdir, stat it */
			if (stat(fullpath, &statbuf) == 0) {
				if (S_ISDIR(statbuf.st_mode)) {
					files_gettracks(fullpath);
				} else {
					files_addtrack(fullpath);
				}
			}
		}
	}
	closedir(curdir);
}

struct track *files_findtrack(char *artist, char *album,
		char *title)
{
	int startpos, endpos, curpos;
	int res;
	struct track track;

	track.artist = artist;
	track.album = album;
	track.title = title;

	startpos = 0;
	endpos = trackcount - 1;
	curpos = (endpos - startpos) / 2;

	while (startpos <= endpos) {
		curpos = (startpos + endpos) / 2;
		res = trackcmp(&track, &tracks[curpos]);

		if (res > 0) {
			startpos = curpos + 1;
		} else if (res < 0) {
			endpos = curpos - 1;
		} else {
			return &tracks[curpos];
		}
	}

	return NULL;
}

void files_buildtracklist(char *dir)
{
	if (tracks == NULL) {
		tracksize = 512;
		trackcount = 0;
		tracks = malloc(sizeof(struct track) * tracksize);
		if (tracks == NULL) {
			fprintf(stderr, "Couldn't allocate track memory\n");
			exit(1);
		}
	}
	files_gettracks(dir);
}

void files_printtracklist(void)
{
	int i;

	for (i = 0; i < trackcount; i++) {
		printf("%-30s %-30s %-30s (%s)\n", tracks[i].artist,
				tracks[i].album,
				tracks[i].title,
				tracks[i].filename);
	}

	return;
}

void files_iterate(void (*itrfunc)(void *ctx, struct track *track), void *ctx)
{
	int i;

	for (i = 0; i < trackcount; i++) {
		itrfunc(ctx, &tracks[i]);
	}

	return;
}
