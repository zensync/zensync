/*
 * Copyright 2012 Jonathan McDowell <noodles@earth.li>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef _FILES_H_
#define _FILES_H_

struct track {
	char	*filename;
	char	*artist;
	char	*album;
	char	*title;
	int	 tracknum;
	off_t	 size;
	int	 seen;
};

/**
 * Find a supplied track (artist/album/title) from the list of scanned files.
 * @param  artist The artist to look for
 * @param  album  The album to look for
 * @param  title  The title to look for
 * @return A pointer to the track structure if found, otherwise NULL.
 */
struct track *files_findtrack(char *artist, char *album, char *title);

/**
 * Build up a list of tracks contained within a directory
 * @param dir The directory to scan for files (includes subdirectories)
 */
void files_buildtracklist(char *dir);

/**
 * Output a list of all tracks currently known to stdout
 */
void files_printtracklist(void);

void files_iterate(void (*itrfunc)(void *ctx, struct track *track), void *ctx);

#endif
