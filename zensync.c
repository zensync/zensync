/*
 * Copyright 2012 Jonathan McDowell <noodles@earth.li>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "files.h"
#include "mtp.h"

#define VERSION "0.0.1"

void sendnewtracks(void *ctx, struct track *track)
{
	if (!track->seen) {
		printf("Sending %s\n", track->filename);
		if (mtp_sendtrack(track) == 0) {
			printf("Sent ok!\n");
			track->seen = 1;
		}
	}
}

void usage(void)
{
	printf("mtpsync " VERSION "\n\n");
	printf("\tmtpsync src [dest]\n\n");
	printf("\tsrc  Directory to scan for music\n");
	printf("\tdest Name of MTP device to sync with. If unspecified uses "
		"the first device found\n");
}

static struct option long_options[] = {
	{ "help", 0, NULL, 0 },
	{ NULL,   0, NULL, 0 }
};

int main(int argc, char *argv[])
{
	int c;
	int option_index;
	int i;
	char *devicename = NULL;

	while ((c = getopt_long(argc, argv, "h", long_options,
			&option_index)) != -1) {
		switch (c) {
		case 'h':
			usage();
			exit(EXIT_SUCCESS);
		default:
			printf("Error: getopt returned 0x%02x\n", c);
			exit(EXIT_FAILURE);
		}
	}

	if (optind >= argc) {
		printf("No source directory supplied.\n");
		exit(EXIT_FAILURE);
	}

	if (argc > (optind + 1)) {
		devicename = argv[--argc];
	}

	if (mtp_init(devicename)) {
		printf("Failed to connect to MTP device.\n");
		exit(EXIT_FAILURE);
	}

	for (i = optind; i < argc; i++) {
		files_buildtracklist(argv[i]);
	}

	files_printtracklist();
	mtp_buildtracklist();

	files_iterate(sendnewtracks, NULL);

	mtp_cleanup();

	exit(EXIT_SUCCESS);
}
