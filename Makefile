LDFLAGS += $(shell pkg-config --libs libmtp taglib_c)
CFLAGS += -Wall -Werror -g $(shell pkg-config --cflags libmtp taglib_c)

zensync: zensync.o files.o mtp.o

clean:
	rm -f *.o zensync
